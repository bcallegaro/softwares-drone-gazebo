#ifndef DRONEPROPULSIONDRIVER_H
#define DRONEPROPULSIONDRIVER_H

/*--- STL Includes. ---*/
#include <math.h>
#include <memory>
#include <vector>

/*--- Gazebo includes. ---*/
#include <sdf/sdf.hh>
#include "gazebo/gazebo.hh"
#include "gazebo/common/PID.hh"
#include "gazebo/physics/physics.hh"
#include "gazebo/transport/transport.hh"


class DronePropulsionDriver
{
public:
    DronePropulsionDriver( gazebo::physics::ModelPtr model );

    void update( const gazebo::common::UpdateInfo &info );

    void setReference( double m1Ref, double m2Ref, double m3Ref, double m4Ref );
private:
    void setForcesAndTorques();
    void setupControllers();

    /*--- class state. ---*/
    gazebo::common::Time m_lastSimTime;

    float m_motor1Ref, m_motor2Ref, m_motor3Ref, m_motor4Ref;
    float m_lastM1Ref, m_lastM2Ref, m_lastM3Ref, m_lastM4Ref;
    float m_motor1Speed, m_motor2Speed, m_motor3Speed, m_motor4Speed;

    uint8_t m_receptionCounter;

    std::vector< gazebo::physics::JointPtr > m_motors;
    std::vector< gazebo::physics::LinkPtr > m_propellers;
    std::vector< gazebo::physics::LinkPtr > m_motorsTop;

    /*--- Speed Controllers. ---*/
    std::vector < gazebo::common::PID > m_motorsControllers;

    /*--- Some Constants. ----*/
        /*--- Constants calculated from http://m-selig.ae.illinois.edu/props/volume-1/data/apcsp_10x4_static_2644rd.txt. ---*/

    static constexpr double PROPELLER_DIAMETER = 10;    //[in]
    static constexpr double AIR_DENSITY = 0.00238; //[slugs/ft³]

    static constexpr double CT = 0.0909;
    static constexpr double CP = 0.0362;

    static constexpr double IN_TO_FEET = 1/12.0;
    static constexpr double POUND_2_NEWTON = 4.44822;
    static constexpr double POUNT_FOOT_2_NEWTON_METER = 1.3558;

    static constexpr double RPM_TO_HZ = 1/60.0;

    static constexpr double MOTOR_THRUST_CONSTANT = CT*AIR_DENSITY*pow( (PROPELLER_DIAMETER*IN_TO_FEET), 4)*POUND_2_NEWTON;
    static constexpr double MOTOR_TORQUE_CONSTANT = CP*AIR_DENSITY*pow( (PROPELLER_DIAMETER*IN_TO_FEET), 5)*POUNT_FOOT_2_NEWTON_METER;

};

#endif // DRONEPROPULSIONDRIVER_H
