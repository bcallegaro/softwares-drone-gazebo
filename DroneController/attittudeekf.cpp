#include "attittudeekf.h"

AttittudeEKF::AttittudeEKF() :
    m_state()
{
    m_state.X(0) = 1;
    m_state.X(1) = 0;
    m_state.X(2) = 0;
    m_state.X(3) = 0;
}

void AttittudeEKF::update( Eigen::Vector3d w, Eigen::Vector3d accel, Eigen::Vector3d mag, double dt, double Qquat, double Raccel, double Rmag )
{
    double wx = w(0);
    double wy = w(1);
    double wz = w(2);

    //Atualizar o estado anterior
    m_state.F <<    1.0, - dt*(wx/2), - dt*(wy/2), - dt*(wz/2),
                    + dt*(wx/2), 1.0, + dt*(wz/2), - dt*(wy/2),
                    + dt*(wy/2), - dt*(wz/2), 1.0, + dt*(wx/2),
                    + dt*(wz/2), + dt*(wy/2), - dt*(wx/2), 1.0;

    m_state.X = m_state.F*m_state.X;

    m_state.X.normalize();

    const double & q0 = m_state.X[0];
    const double & q1 = m_state.X[1];
    const double & q2 = m_state.X[2];
    const double & q3 = m_state.X[3];

    double q11, q12, q13, q14;
    double q21, q22, q23, q24;
    double q31, q32, q33, q34;
    double q41, q42, q43, q44;

    double sqrDt = powf(dt, 2);

    q11 =  (Qquat*sqrDt*pow(q1,2))/4 + (Qquat*sqrDt*pow(q2,2))/4 + (Qquat*sqrDt*pow(q3,2))/4;
    q12 = -(Qquat*sqrDt*q0*q1)/4;
    q13 = -(Qquat*sqrDt*q0*q2)/4;
    q14 = -(Qquat*sqrDt*q0*q3)/4;

    q21 = -(Qquat*sqrDt*q0*q1)/4;
    q22 =  (Qquat*sqrDt*pow(q0,2))/4 + (Qquat*sqrDt*pow(q2,2))/4 + (Qquat*sqrDt*pow(q3,2))/4;
    q23 = -(Qquat*sqrDt*q1*q2)/4;
    q24 = -(Qquat*sqrDt*q1*q3)/4;

    q31 = -(Qquat*sqrDt*q0*q2)/4;
    q32 = -(Qquat*sqrDt*q1*q2)/4;
    q33 =  (Qquat*sqrDt*pow(q0,2))/4 + (Qquat*sqrDt*pow(q1,2))/4 + (Qquat*sqrDt*pow(q3,2))/4;
    q34 = -(Qquat*sqrDt*q2*q3)/4;

    q41 = -(Qquat*sqrDt*q0*q3)/4;
    q42 = -(Qquat*sqrDt*q1*q3)/4;
    q43 = -(Qquat*sqrDt*q2*q3)/4;
    q44 =  (Qquat*sqrDt*pow(q0,2))/4 + (Qquat*sqrDt*pow(q1,2))/4 + (Qquat*sqrDt*pow(q2,2))/4;

    m_state.q <<    q11, q12, q13, q14,
                    q21, q22, q23, q24,
                    q31, q32, q33, q34,
                    q41, q42, q43, q44;

    m_state.p = m_state.F*m_state.p*m_state.F.transpose() + m_state.q;

    const Eigen::Matrix<double, 3, 3> rotationMatrix( getRotMatFromQuaternion( q0, q1, q2, q3 ) );

    /*--- Comparando. ---*/
    m_state.measures << rotationMatrix*m_accelRef, rotationMatrix*m_magRef;

    m_state.z << accel, mag;
    m_state.y = m_state.z - m_state.measures;

    double &gx = m_accelRef(0);  double &gy = m_accelRef(1);  double &gz = m_accelRef(2);
    double &hx = m_magRef(0);    double &hy = m_magRef(1);    double &hz = m_magRef(2);

    m_state.H <<
#if defined( THALES )
                 2*gy*q3 - 2*gz*q2,           2*gy*q2 + 2*gz*q3, 2*gy*q1 - 4*gx*q2 - 2*gz*q0, 2*gy*q0 - 4*gx*q3 + 2*gz*q1,
                 2*gz*q1 - 2*gx*q3, 2*gx*q2 - 4*gy*q1 + 2*gz*q0,           2*gx*q1 + 2*gz*q3, 2*gz*q2 - 4*gy*q3 - 2*gx*q0,
                 2*gx*q2 - 2*gy*q1, 2*gx*q3 - 2*gy*q0 - 4*gz*q1, 2*gx*q0 + 2*gy*q3 - 4*gz*q2,           2*gx*q1 + 2*gy*q2,
                 2*hy*q3 - 2*hz*q2,           2*hy*q2 + 2*hz*q3, 2*hy*q1 - 4*hx*q2 - 2*hz*q0, 2*hy*q0 - 4*hx*q3 + 2*hz*q1,
                 2*hz*q1 - 2*hx*q3, 2*hx*q2 - 4*hy*q1 + 2*hz*q0,           2*hx*q1 + 2*hz*q3, 2*hz*q2 - 4*hy*q3 - 2*hx*q0,
                 2*hx*q2 - 2*hy*q1, 2*hx*q3 - 2*hy*q0 - 4*hz*q1, 2*hx*q0 + 2*hy*q3 - 4*hz*q2,           2*hx*q1 + 2*hy*q2;
#elif defined(SABATINI_2011)
                   4*gx*pow(q0,3) + 2*gy*q3 - 2*gz*q2, 2*gx*q1 + 2*gy*q2 + 2*gz*q3, 2*gy*q1 - 2*gx*q2 - 2*gz*q0, 2*gy*q0 - 2*gx*q3 + 2*gz*q1,
                   2*gy*q0 - 2*gx*q3 + 2*gz*q1, 2*gx*q2 - 2*gy*q1 + 2*gz*q0, 2*gx*q1 + 2*gy*q2 + 2*gz*q3, 2*gz*q2 - 2*gy*q3 - 2*gx*q0,
                   2*gx*q2 - 2*gy*q1 + 2*gz*q0, 2*gx*q3 - 2*gy*q0 - 2*gz*q1, 2*gx*q0 + 2*gy*q3 - 2*gz*q2, 2*gx*q1 + 2*gy*q2 + 2*gz*q3,
                   4*hx*pow(q0,3) + 2*hy*q3 - 2*hz*q2, 2*hx*q1 + 2*hy*q2 + 2*hz*q3, 2*hy*q1 - 2*hx*q2 - 2*hz*q0, 2*hy*q0 - 2*hx*q3 + 2*hz*q1,
                   2*hy*q0 - 2*hx*q3 + 2*hz*q1, 2*hx*q2 - 2*hy*q1 + 2*hz*q0, 2*hx*q1 + 2*hy*q2 + 2*hz*q3, 2*hz*q2 - 2*hy*q3 - 2*hx*q0,
                   2*hx*q2 - 2*hy*q1 + 2*hz*q0, 2*hx*q3 - 2*hy*q0 - 2*hz*q1, 2*hx*q0 + 2*hy*q3 - 2*hz*q2, 2*hx*q1 + 2*hy*q2 + 2*hz*q3;
#endif

    m_state.r << KalmanFilterState::MatrixAA::Identity();

    for( int i = 0; i < 3; ++i ) {
        m_state.r( i, i ) *= Raccel;
    }

    for( int i = 3; i < 6; ++i ) {
        m_state.r( i,i ) *= Rmag;
    }

    m_state.k = (m_state.p*m_state.H.transpose())*((m_state.H*m_state.p*m_state.H.transpose()+ m_state.r).inverse());

    m_state.X = m_state.X + ( m_state.k*m_state.y );

    m_state.X.normalize();

    m_state.p = m_state.p - ( m_state.k*m_state.H*m_state.p );
}

Eigen::Matrix<double, 3, 3> getRotMatFromQuaternion( const double & q0, const double & q1, const double & q2, const double & q3  ) {

    Eigen::Matrix<double, 3, 3> rotationMatrix;
#if defined(THALES)
    rotationMatrix <<   - 2*pow(q2,2) - 2*pow(q3,2) + 1,     2*q0*q3 + 2*q1*q2,     2*q1*q3 - 2*q0*q2,
                        2*q1*q2 - 2*q0*q3, - 2*pow(q1,2) - 2*pow(q3,2) + 1,     2*q0*q1 + 2*q2*q3,
                        2*q0*q2 + 2*q1*q3,     2*q2*q3 - 2*q0*q1, - 2*pow(q1,2) - 2*pow(q2,2) + 1;

#elif defined(SABATINI_2011)
    rotationMatrix <<   pow(q0,4) + pow(q1,2) - pow(q2,2) - pow(q3,2),  2*q0*q3 + 2*q1*q2,  2*q1*q3 - 2*q0*q2,
                        2*q1*q2 - 2*q0*q3,  pow(q0,2) - pow(q1,2) + pow(q2,2) - pow(q3,2),  2*q0*q1 + 2*q2*q3,
                        2*q0*q2 + 2*q1*q3,  2*q2*q3 - 2*q0*q1, pow(q0,2) - pow(q1,2) - pow(q2,2) + pow(q3,2);

#endif

    return rotationMatrix;


}
