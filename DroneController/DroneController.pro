#-------------------------------------------------
#
# Project created by QtCreator 2016-08-28T23:08:02
#
#-------------------------------------------------

QT += widgets charts

CONFIG += C++11

CONFIG += link_pkgconfig

TARGET = DroneController
TEMPLATE = app

SOURCES += main.cpp \
    dronecontrollergui.cpp \
    ../messages/MotorControlMessage.pb.cc \
    ../../../joystick/joystick.cc \
    attittudeekf.cpp    


HEADERS  += dronecontrollergui.h \
    ../messages/MotorControlMessage.pb.h \
    ../../../joystick/joystick.hh \
    attittudeekf.h

FORMS    += dronecontrollergui.ui

DESTDIR += ../build

# Messages
INCLUDEPATH += ../messages
DEPENDPATH  += ../messages

#Eigen include path
INCLUDEPATH += $(HOME)/dev/eigen

#--- Signal processing libs include and src paths.
INCLUDEPATH += $(HOME)/dev/signalprocessors
DEPENDPATH  += $(HOME)/dev/signalprocessors

# Signals processings libs path.
LIBS += -L$(HOME)/dev/signalprocessors
LIBS += -lSignalProcessors

#Catkin Dirs
#CATKIN_LIBS_DIR = /usr/lib/x86_64-linux-gnu
CATKIN_INCLUDE_DIR = /usr/include/
CATKIN_GAZEBO_INCLUDE_DIR = /usr/include/gazebo-8

#Gazebo include pahts.
INCLUDEPATH += $${CATKIN_GAZEBO_INCLUDE_DIR}/
INCLUDEPATH += $${CATKIN_GAZEBO_INCLUDE_DIR}/gazebo/transport
INCLUDEPATH += $${CATKIN_GAZEBO_INCLUDE_DIR}/gazebo/msgs/
INCLUDEPATH += $${CATKIN_INCLUDE_DIR}/ignition/msgs0/
INCLUDEPATH += $${CATKIN_INCLUDE_DIR}/ignition/math3

PKGCONFIG += sdformat

#Library path for catkin compiled librarys
LIBS += -L$${CATKIN_LIBS_DIR}

#Including alglib library
LIBS += -lalglib

#Ignition includes
LIBS += -lignition-math3
LIBS += -lignition-transport3

#Boost includes
LIBS += -lboost_system
LIBS += -lboost_thread
LIBS += -lboost_filesystem
LIBS += -lboost_iostreams
LIBS += -lboost_regex

#Protobuf includes
LIBS += -lprotobuf

#Gazebo libraries
LIBS += -lgazebo
LIBS += -lgazebo_client
LIBS += -lgazebo_transport
LIBS += -lgazebo_msgs
LIBS += -lgazebo_common
LIBS += -lgazebo_math
LIBS += -lgazebo_util
