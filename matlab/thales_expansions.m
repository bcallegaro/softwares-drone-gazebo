clear all;
close all;
clc;

NUMBER_OF_STATES = 4;
NUMBER_OF_MEASURES = 6;
NUMBER_OF_INPUTS = 0;

syms gx gy gz ax ay az hx hy hz mx my mz qw qx qy qz wx wy wz Ts cov_g cov_a cov_h real;

%% Vetor com a referência da gravidade
g = [gx; gy; gz];

%% Vetor com a referência do campo magnético
h = [hx; hy; hz];

%% Vetor com as medidas dos sensores da IMU
omega = [wx; wy; wz];   %Giroscópio.q_dot = skew(omega)
a = [ax; ay; az];       %Acelerômetro.q_dot = skew(omega)
m = [mx; my; mz];       %Magnetômetro.

%% Definição do quartenion
q_vec = [qx; qy; qz];
q = [qw; q_vec];

%% Definindo as matriz de atualização dos estados.

%% - Original (Contem erros )
Omega = 0.5*[   0       -omega'; 
                omega   skew(omega) ];
            
%% - Versão modificada (Utilizada atualmente)
Omega = 0.5*[   0       -omega'; 
                omega   -skew(omega) ];


F_k = eye(NUMBER_OF_STATES) + Omega*Ts;

%% - Matriz de covariância do processo de atualização.
Xi_k = [    -q_vec'; 
            skew(q_vec)+qw*eye(3)];

Sigma_g = eye(3)*cov_g;
        
Qk = ((Ts/2)^2)*Xi_k*Sigma_g*Xi_k';

%% - Processos de observação dos estados.
C_mf = eye(3) - 2*qw*skew(q_vec) + 2*skew(q_vec)*skew(q_vec); %Matriz de rotação

d_mf_dq_g = [ -2*skew(q_vec)*g 2*qw*skew(g) + 2*(skew(g)*skew(q_vec)-2*skew(q_vec)*skew(g)) ];
d_mf_dq_h = [ -2*skew(q_vec)*h 2*qw*skew(h) + 2*(skew(h)*skew(q_vec)-2*skew(q_vec)*skew(h)) ];

H_k = [ d_mf_dq_g;
        d_mf_dq_h ];
        