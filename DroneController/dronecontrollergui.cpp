#include <QtWidgets>

#include "dronecontrollergui.h"
#include "ui_dronecontrollergui.h"

#if defined(CHARTS_ENABLED)
#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QtCharts/QChart>
#include <QtWidgets/QVBoxLayout>
#include <QtCharts/QValueAxis>
#endif

DroneControllerGUI::DroneControllerGUI(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DroneControllerGUI)
  , m_altitudeRef( 0.0 )
  , m_rollRef( 0.0 )
  , m_pitchRef( 0.0 )
  , m_yawRef( 0.0 )
  , m_magnetometer( 0.0, 0.0, 0.0 )
  , m_zeroMessage()
  , m_gyroFilter( new FIRFilter< gazebo::msgs::Vector3d >( FILTER_TAP_NUM ) )
  , m_accelFilter( new FIRFilter< gazebo::msgs::Vector3d>( FILTER_TAP_NUM ) )
  , m_controllersInitialized( false )
  , m_pidAttitudeControllers( )
  , m_lqrAttitudeControllers( )
  , m_altimterLastTs()
  , m_imuLastTs()
  , m_firAttitudeEkf()
  , m_rawAttitudeEkf()
  , m_firstAccelReceived( false )
  , m_firstMagReceived( false )
  , m_filtersInitialized( false )
  , m_joystickTimerId( -1 )
  , m_joystick( new Joystick( 0 ) )
#if defined(CHARTS_ENABLED)
  , m_refChartView()
  , m_curChartView()
  , m_rollRefSeries()
  , m_pitchRefSeries()
  , m_yawRefSeries()
  , m_rollEstSeries()
  , m_pitchEstSeries()
  , m_yawEstSeries()
#endif
{
    ui->setupUi(this);

    gazebo::client::setup();
    gazebo::transport::NodePtr node = gazebo::transport::NodePtr(new gazebo::transport::Node());
    node->Init();

#if defined( DRONE )
    m_motorPublisher = node->Advertise< MotorControlMessage >( "~/drone/motors" );
    m_motorPublisher->WaitForConnection();

    m_imuSubs = node->Subscribe( "~/drone_v1/control_board/imu/imu", &DroneControllerGUI::newIMUDataReceived, this );
    m_altimeterSubs = node->Subscribe( "altimeter", &DroneControllerGUI::newAltimeterDataReceived, this );
    m_magnetometerSubs = node->Subscribe( "~/drone_v1/control_board/HCM5883/magnetometer", &DroneControllerGUI::newMagnetometerDataReceived, this );

#elif defined( INERTIAL_PLATFORM )
    m_imuSubs = node->Subscribe( "~/inertial_test_2/link_8/imu/imu", &DroneControllerGUI::newIMUDataReceived, this );
    m_magnetometerSubs = node->Subscribe( "~/inertial_test_2/link_8/HCM5883/magnetometer", &DroneControllerGUI::newMagnetometerDataReceived, this );
#endif

#if defined(CHARTS_ENABLED)
    {
        m_refChartView = new QChartView( new QChart() );
        m_refChartView->setRenderHint( QPainter::Antialiasing );
        ui->plotVerticalLayout->addWidget( m_refChartView );

        //Ajustando gráfico de referência.
        m_refChartView->chart()->legend()->hide();

        QValueAxis *axis = new QValueAxis();

        m_refChartView->chart()->setTitle( "Orientação referência" );
        m_refChartView->chart()->setTitleFont( QFont("Helvetica", 15, 15, false) );        

        m_rollRefSeries = new QLineSeries( m_refChartView->chart() );
        m_pitchRefSeries = new QLineSeries( m_refChartView->chart() );
        m_yawRefSeries = new QLineSeries( m_refChartView->chart() );

        m_rollEstSeries = new QLineSeries( m_refChartView->chart() );
        m_pitchEstSeries = new QLineSeries( m_refChartView->chart() );
        m_yawEstSeries = new QLineSeries( m_refChartView->chart() );

        QPen green( Qt::green );
        QPen red( Qt::red );
        QPen black( Qt::black );
        QPen blue( Qt::blue );
        QPen darkBlue( Qt::darkBlue );
        QPen gray( Qt::gray );

        green.setWidth( 1.0f );
        red.setWidth( 1.0f );
        black.setWidth( 1.0f );
        blue.setWidth( 1.0f );
        darkBlue.setWidth( 1.0f );
        gray.setWidth( 1.0f );

        m_rollRefSeries->setPen( green );
        m_pitchRefSeries->setPen( red );
        m_yawRefSeries->setPen( gray );
        m_rollEstSeries->setPen( black );
        m_pitchEstSeries->setPen( blue );
        m_yawEstSeries->setPen( darkBlue );

        m_rollRefSeries->append( 0.0, 0.0 );
        m_pitchRefSeries->append( 0.0, 0.0 );
        m_yawRefSeries->append( 0.0, 0.0 );
        m_rollEstSeries->append( 0.0, 0.0 );
        m_pitchEstSeries->append( 0.0, 0.0 );
        m_yawEstSeries->append( 0.0, 0.0 );

        m_refChartView->chart()->addSeries( m_rollRefSeries );
        m_refChartView->chart()->addSeries( m_pitchRefSeries );
        m_refChartView->chart()->addSeries( m_yawRefSeries );
        m_refChartView->chart()->addSeries( m_rollEstSeries );
        m_refChartView->chart()->addSeries( m_pitchEstSeries );
        m_refChartView->chart()->addSeries( m_yawEstSeries );

        m_refChartView->chart()->setAxisX( axis, m_rollRefSeries );
        m_refChartView->chart()->setAxisX( axis, m_pitchRefSeries );
        m_refChartView->chart()->setAxisX( axis, m_yawRefSeries );
        m_refChartView->chart()->setAxisX( axis, m_rollEstSeries );
        m_refChartView->chart()->setAxisX( axis, m_pitchEstSeries );
        m_refChartView->chart()->setAxisX( axis, m_yawEstSeries );

        m_rollRefSeries->setName("Roll de referência");
        m_pitchRefSeries->setName("Pitch de referência");
        m_yawRefSeries->setName("Yaw Referência");

        m_rollEstSeries->setName("Roll estimado");
        m_pitchEstSeries->setName("Pitch estimado");
        m_yawEstSeries->setName("YAw estimado");

        m_refChartView->chart()->legend()->setVisible( true );

        axis->setTickCount( 6 );
        m_refChartView->chart()->createDefaultAxes();
        m_refChartView->setRenderHint( QPainter::Antialiasing );

        m_refChartView->chart()->axisX()->setRange( 0, 30 );
        m_refChartView->chart()->axisY()->setRange( -20, 20 );

        /*--- Ajustando valores estimados. ---*/
//        m_curChartView = new QChartView( new QChart() );
//        m_curChartView->setRenderHint( QPainter::Antialiasing );
//        ui->plotVerticalLayout->addWidget( m_curChartView );

//        m_curChartView->chart()->legend()->hide();

//        axis = new QValueAxis();

//        m_curChartView->chart()->setTitle( "Orientação estimada" );
//        m_curChartView->chart()->setTitleFont( QFont("Helvetica", 15, 15, false) );

//        m_rollEstSeries = new QLineSeries( m_curChartView->chart() );
//        m_pitchEstSeries = new QLineSeries( m_curChartView->chart() );

//        m_rollEstSeries->setPen( green );
//        m_rollEstSeries->append( 0.0, 0.0 );
//        m_curChartView->chart()->addSeries( m_rollEstSeries );

//        m_pitchEstSeries->setPen( red );
//        m_pitchEstSeries->append( 0.0, 0.0 );
//        m_curChartView->chart()->addSeries( m_pitchEstSeries );

//        m_curChartView->chart()->createDefaultAxes();

//        m_curChartView->chart()->setAxisX( axis, m_rollEstSeries );
//        m_curChartView->chart()->setAxisX( axis, m_pitchEstSeries );

//        axis->setTickCount( 6 );

//        m_curChartView->setRenderHint( QPainter::Antialiasing );

//        m_curChartView->chart()->axisX()->setRange( 0, 30 );
//        m_curChartView->chart()->axisY()->setRange( -20, 20 );
    }
#endif

    /*--- Setting up attitude controllers. ---*/
    m_pidAttitudeControllers.resize(4);                                          //Roll, pitch and yaw.

    for( int i = 0; i < 4; ++i ) {
        m_pidAttitudeControllers[i].resize(3);

        initPIDControllerState( &m_pidAttitudeControllers[i][ ROLL ],  0.028, 0.0015, 0*0.04, 127, 0.0025 );
        initPIDControllerState( &m_pidAttitudeControllers[i][ PITCH ], 0.028, 0.0015, 0*0.04, 127, 0.0025 );
        initPIDControllerState( &m_pidAttitudeControllers[i][ YAW ],   0.028, 0.0015, 0*0.04, 127, 0.0025 );
    }

    /*--- Setting up height PID controller. ---*/
    m_altitudeController.Kp = 1.2;
    m_altitudeController.Kd = 0.8;
    m_altitudeController.Ki = 0.001;
    m_altitudeController.N = 100;

    m_altitudeController.ek1 = 0.0;
    m_altitudeController.ek2 = 0.0;
    m_altitudeController.uk1 = 0.0;
    m_altitudeController.uk2 = 0.0;

    m_altitudeController.Ts = 0.1;

    /*--- Setting up filter. ---*/
    std::vector< float > coef;

    coef.assign( filter_taps, filter_taps + FILTER_TAP_NUM );

    m_gyroFilter->insertFilterCoef( coef );
    m_accelFilter->insertFilterCoef( coef );

    m_lqrAttitudeControllers.resize(1);

    m_lqrAttitudeControllers[0].kMatrix <<      5.1116,        0,        0,   0.3248,        0,        0,   1.0000,        0,        0,
                                                0,   5.1116,        0,        0,   0.3248,        0,        0,   1.0000,        0,
                                                0,        0,   6.7092,        0,        0,   0.4022,        0,        0,   1.0000;


    m_controllersInitialized = true;

#if defined(DRONE)
    /*-- Starting up the timers. ---*/
    m_joystickTimerId = startTimer( 50, Qt::PreciseTimer );

    /*--- setting up joystick. ---*/
    if( !m_joystick->isFound() ) {
        qFatal("erro ao encontrar joystick");
    }
#endif

    for( int i = 0; i < 4; ++i ) {
        m_firAttitudeEkf.updateCovMatrix()(i,i) = 1e-3;
    }

}

DroneControllerGUI::~DroneControllerGUI()
{
    //gazebo::shutdown();
    delete ui;
}


gazebo::msgs::Quaternion DroneControllerGUI::updateEKF( gazebo::msgs::IMU & imu, Eigen::Vector3d & mag, double dt, AttittudeEKF & filter, bool motorsEnabled ) {
    Eigen::Vector3d w, accel, scaledMag;

    gazebo::msgs::Quaternion q;

    w(0) =  imu.mutable_angular_velocity()->x();
    w(1) =  imu.mutable_angular_velocity()->y();
    w(2) =  imu.mutable_angular_velocity()->z();


    accel(0) = imu.linear_acceleration().x()/9.81;
    accel(1) = imu.linear_acceleration().y()/9.81;
    accel(2) = imu.linear_acceleration().z()/9.81;

    double rAccel = 3e-4;

    motorsEnabled ? rAccel = 1e-3 : rAccel = 1e-6;

    filter.update( w, accel, mag, dt, 1e-6, rAccel, rAccel );

    q.set_w( filter.state().X(0) );
    q.set_x( filter.state().X(1) );
    q.set_y( filter.state().X(2) );
    q.set_z( filter.state().X(3) );

    return q;
}

void DroneControllerGUI::newIMUDataReceived(ConstIMUPtr &imuData)
{
    static uint8_t attitudeReplotCounter = 0;
    static uint8_t attitudeReplotCounter_2 = 0;

    double time_sec = imuData->stamp().nsec()/1e9 + imuData->stamp().sec();
    static const double startAttitudeTimer = time_sec;

    if( ! m_filtersInitialized ) {

        /*--- Supondo que, a orientação inciial seja conhecida, nem sempre verdade, mas assumiremos para os testes. ---*/
        m_firAttitudeEkf.rState().X(0) = imuData->orientation().w();
        m_firAttitudeEkf.rState().X(1) = imuData->orientation().x();
        m_firAttitudeEkf.rState().X(2) = imuData->orientation().y();
        m_firAttitudeEkf.rState().X(3) = imuData->orientation().z();

        /*--- Sabemos que o vetor de campo gravitacional é bem comportado, e sempre 1, ou seja, assumiremos isso par o filtro. ---*/

        m_firstAccelReceived = true;
        m_firAttitudeEkf.m_accelRef(0) = 0;
        m_firAttitudeEkf.m_accelRef(1) = 0;
        m_firAttitudeEkf.m_accelRef(2) = 1;

        if( m_firstMagReceived ) {
            m_firAttitudeEkf.m_magRef(0) = m_magnetometer(0);
            m_firAttitudeEkf.m_magRef(1) = m_magnetometer(1);
            m_firAttitudeEkf.m_magRef(2) = m_magnetometer(2);

            m_filtersInitialized = true;
        }
    }


#if defined(ROTATE_REF_VECTOR)
    gazebo::math::Vector3 accel_vector( imuData->linear_acceleration().x(), imuData->linear_acceleration().y(), imuData->linear_acceleration().z() );
    gazebo::math::Quaternion refq( imuData->orientation().w(), imuData->orientation().x(), imuData->orientation().y(), imuData->orientation().z() );
    gazebo::math::Vector3 accel_vector_rotated( refq.RotateVectorReverse( gazebo::math::Vector3(0, 0, 9.81) ) );

    gazebo::msgs::Vector3d accel_vector2;
    accel_vector2.set_x( accel_vector_rotated.x );
    accel_vector2.set_y( accel_vector_rotated.y );
    accel_vector2.set_z( accel_vector_rotated.z );

    //*m_filteredIMU.mutable_angular_velocity() = m_gyroFilter->addElementAndProcess( imuData->angular_velocity() );
    *m_filteredIMU.mutable_angular_velocity() = imuData->angular_velocity();
    *m_filteredIMU.mutable_linear_acceleration() = m_accelFilter->addElementAndProcess( accel_vector2 );
#elif defined(RAW_SIMULATED_SOURCE)
    *m_filteredIMU.mutable_angular_velocity() = imuData->angular_velocity();
    *m_filteredIMU.mutable_linear_acceleration() = m_accelFilter->addElementAndProcess( imuData->linear_acceleration() );
#endif

    m_rawIMU = *imuData;

    /*---- Attitude stability controle. ----*/
    gazebo::common::Time time( imuData->stamp().sec(), imuData->stamp().nsec() );

    double dt = (time - m_imuLastTs).nsec/(1e9);
    const gazebo::msgs::Quaternion ref_q = imuData->orientation();

    /*--- Não faz sentido continuar com o filtro não inicializado corretamente. ---*/
    if( !m_filtersInitialized ) return;

    /*--- Kalman Filter for attitude estimation. ---*/
    const gazebo::msgs::Quaternion raw_q( updateEKF( m_rawIMU, m_magnetometer, dt, m_rawAttitudeEkf, m_altitudeRef > 0 ) );
    const gazebo::msgs::Quaternion fir_q( updateEKF( m_filteredIMU, m_magnetometer, dt, m_firAttitudeEkf, m_altitudeRef > 0 ) );


#if defined(CHARTS_ENABLED)
        plot3DVectorData( m_refChartView, m_rollRefSeries, m_pitchRefSeries, m_yawRefSeries, quaternion2EulerAngles( ref_q ),  time_sec, startAttitudeTimer, attitudeReplotCounter );
        plot3DVectorData( m_refChartView, m_rollEstSeries, m_pitchEstSeries, m_yawEstSeries, quaternion2EulerAngles( fir_q ), time_sec, startAttitudeTimer, attitudeReplotCounter_2 );
#endif

    /*----------------------------------------------*/
    m_imuLastTs = time;



#if defined(PID)
    MotorCommands refControllerComands = getPIDControlOutputs( ref_q, *imuData, RefAttitudeController, dt );
    MotorCommands firEkfControllerComands = getPIDControlOutputs( fir_q, m_filteredIMU, FirEkfAttitudeController, dt );
    MotorCommands rawEkfControllerComands = getPIDControlOutputs( raw_q, *imuData, RawEkfAttitudeController, dt );
    //MotorCommands cfControllerComands = getPIDControlOutputs( q3, imuData, EkfAttitudeController );
#elif defined(LQRI)
    MotorCommands firEkfControllerComands = getLQRIControlOutputs( fir_q, m_filteredIMU, RawEkfAttitudeController, dt );
#endif

    MotorCommands adEkfControllerComands = getPIDControlOutputs( fir_q, m_filteredIMU, FirEkfAttitudeController, dt );

    MotorControlMessage cmd = inserir_ajuster_motores( firEkfControllerComands, m_altitudeCmd );

#if defined(DRONE)
    if( m_altitudeRef > 100 )
        m_motorPublisher->Publish( cmd );
    else {
        m_zeroMessage.clear_motor1();
        m_zeroMessage.clear_motor2();
        m_zeroMessage.clear_motor3();
        m_zeroMessage.clear_motor4();

        m_motorPublisher->Publish( m_zeroMessage );
    }
#endif

#if 0
    /*--- Just for testing. ---*/
    double pitch = atan2( -m_filteredIMU.linear_acceleration().x(), m_filteredIMU.linear_acceleration().z() );
    double roll = atan2( m_filteredIMU.linear_acceleration().y(), sqrt( pow(m_filteredIMU.linear_acceleration().x(),2) + pow(m_filteredIMU.linear_acceleration().z(),2) ) );

    std::cout << "roll: " << roll << " pitch: " << pitch << std::endl;

    /*--- Calculando o vetor magnético. ---*/
    double bfx = m_magnetometer(0)*cos(pitch) + m_magnetometer(1)*sin(pitch)*sin(roll) + m_magnetometer(2)*sin(pitch)*cos(roll);
    double bfy = m_magnetometer(1)*cos(roll) - m_magnetometer(2)*sin(roll);
    double bfz = -m_magnetometer(0)*sin(pitch) + m_magnetometer(1)*cos(pitch)*sin(roll) + m_magnetometer(2)*cos(pitch)*cos(roll);


    std::cout << " bfx: " << bfx << " bfy: " << bfy << " bfz: " << bfz << std::endl;
    std::cout << " yaw: " << 57.3*atan2( -bfy, bfx ) << std::endl;
#endif

}

void DroneControllerGUI::newAltimeterDataReceived(ConstAltimeterPtr &altitude)
{
    if( ! m_controllersInitialized ) return;

    static int counter = 0;

    counter ++;
    m_altitude += (altitude->vertical_position());

    if( counter == 10 ) {
        m_altitude /= 10.0;
        double error = (m_altitudeRef/100.0) - m_altitude;

        m_altitudeCmd = updatePIDController( &m_altitudeController, error, 0.1 );

        m_altitudeCmd > 10000.0f ? m_altitudeCmd = 10000.0f : m_altitudeCmd = m_altitudeCmd;
        m_altitudeCmd < 0 ? m_altitudeCmd = 0 : m_altitudeCmd = m_altitudeCmd;

        m_altitude = 0.0f;

        counter = 0;
    }
}

void DroneControllerGUI::newMagnetometerDataReceived(ConstMagnetometerPtr &magnetometer)
{
    if( !m_firstMagReceived ) m_firstMagReceived = true;

    m_magnetometer(0) = ( magnetometer->field_tesla().x() );
    m_magnetometer(1) = ( magnetometer->field_tesla().y() );
    m_magnetometer(2) = ( magnetometer->field_tesla().z() );

}

MotorCommands DroneControllerGUI::getPIDControlOutputs(const gazebo::msgs::Quaternion &ref, const gazebo::msgs::IMU &imuData, Controller controller, double dt )
{
    MotorCommands command;

    const EulerAngles e( quaternion2EulerAngles( ref ) );

    RateRef rateRef;
    rateRef.x = ( m_pitchRef - e.pitch )*3.5 ;
    rateRef.y = ( m_rollRef - e.roll  )*3.5 ;
    rateRef.z = constrainAngle( m_yawRef - e.yaw )*3.5;

    Rate rateError;
    rateError.x = rateRef.x - imuData.angular_velocity().y();
    rateError.y = rateRef.y - imuData.angular_velocity().x();
    rateError.z = rateRef.z - imuData.angular_velocity().z();


    if( ! m_controllersInitialized ) return command;

    double rollCmd = updatePIDController(   &m_pidAttitudeControllers[controller][ ROLL ], rateError.x, dt );
    double pitchCmd = updatePIDController(  &m_pidAttitudeControllers[controller][ PITCH ], rateError.y, dt );
    double yawCmd = updatePIDController(    &m_pidAttitudeControllers[controller][ YAW ], rateError.z, dt );

    command.rollCommand = rollCmd;
    command.pitchCommand = pitchCmd;
    command.yawCommand = yawCmd;

//    static int counter =0 ;
//    if( ++counter == 400 ) {
//    std::cout << std::endl << "PID" << std::endl;
//    std::cout << "rollCmd: " << command.rollCommand << " pitchCmd: " << command.pitchCommand << " yawCmd: " << command.yawCommand << std::endl;
//    counter = 0;
//    }

    return command;
}

MotorCommands DroneControllerGUI::getLQRIControlOutputs(const gazebo::msgs::Quaternion &ref, const gazebo::msgs::IMU &imuData , Controller controller, double dt) {
    MotorCommands command;

    const EulerAngles e( quaternion2EulerAngles( ref ) );


    if( ! m_controllersInitialized ) return command;

    double rollCmd, pitchCmd, yawCmd;
    rollCmd = pitchCmd = yawCmd = 0.0;

    LQRStateVector stateVector;

    stateVector[0] = m_pitchRef - e.pitch;
    stateVector[1] = m_rollRef - e.roll;
    stateVector[2] = m_yawRef - e.yaw;

    stateVector[3] = 0 - imuData.angular_velocity().y();
    stateVector[4] = 0 - imuData.angular_velocity().x();
    stateVector[5] = 0 - imuData.angular_velocity().z();

    stateVector[6] = m_lqrAttitudeControllers[0].iState[0] + stateVector[0]*dt;
    stateVector[7] = m_lqrAttitudeControllers[0].iState[1] + stateVector[1]*dt;
    stateVector[8] = m_lqrAttitudeControllers[0].iState[2] + stateVector[2]*dt;

    m_lqrAttitudeControllers[0].iState  <<  stateVector[6],
                                            stateVector[7],
                                            stateVector[8];


    LQRIOutput output = m_lqrAttitudeControllers[0].kMatrix*stateVector;

    command.rollCommand = output[0];
    command.pitchCommand = output[1];
    command.yawCommand = output[2];

//    static int counter = 0;
//    if( ++counter == 400 ) {
//    std::cout << std::endl << "LQR" << std::endl;
//    std::cout << "rollCmd: " << command.rollCommand << " pitchCmd: " << command.pitchCommand << " yawCmd: " << command.yawCommand << std::endl;
//    counter = 0;
//    }

    return command;
}

void DroneControllerGUI::timerEvent(QTimerEvent *e)
{
    static double altitudeRef = 0.0;
    static double yawRef = 0.0;

    if( e->timerId() == m_joystickTimerId) {

        JoystickEvent event;
        while( m_joystick->sample(&event) ) {
            if( event.isAxis() ) {

                if( event.number == 0 ) {
                    yawRef = -0.1*0.2618*event.value/32767.0;
                }

                if( event.number == 1 ) {
                    altitudeRef = 5.0*event.value/32767.0;
                }

                if ( event.number == 2 ) {
                    m_rollRef = -0.2618*event.value/32767.0;;
                }

                if ( event.number == 3 ) {
                    m_pitchRef = 0.2618*event.value/32767.0;;
                }
            }
        }
    }

    m_yawRef += yawRef;
    m_altitudeRef += altitudeRef;
}

#if defined(CHARTS_ENABLED)
void DroneControllerGUI::plot3DVectorData( QtCharts::QChartView *plot,
                                           QLineSeries *roll,
                                           QLineSeries *pitch,
                                           QLineSeries *yaw,
                                           const EulerAngles &angles, double time, double startTime, uint8_t & replotCounter )
{
    ++replotCounter;

    if( ( replotCounter >= 10 ) ) {
        QtCharts::QValueAxis *axis = reinterpret_cast< QtCharts::QValueAxis *> ( plot->chart()->axisX() );

        qreal dt = ( (time-startTime) - roll->points().back().x() );
        qreal x = plot->chart()->plotArea().width() * dt/( axis->max() - axis->min() );

        roll->append( (time-startTime) , angles.roll*57.3 );
        pitch->append( (time-startTime) , angles.pitch*57.3 );
        yaw->append( (time-startTime), angles.yaw*57.3/9.0 );

        if( ( time - startTime ) > axis->max() ) {
            plot->chart()->scroll ( x ,0 );

            QVector< QPointF > rollPoints, pitchPoints, yawPoints;

            foreach( const QPointF &p, roll->pointsVector() ) {
                if( p.x() >= (axis->max()-30.0) ) {
                    rollPoints.append( p );
                }
            }

            foreach( const QPointF &p, pitch->pointsVector() ) {
                if( p.x() >= (axis->max()-30.0) ) {
                    pitchPoints.append( p );
                }
            }

            foreach( const QPointF &p, yaw->pointsVector() ) {
                if( p.x() >= (axis->max()-30.0) ) {
                    yawPoints.append( p );
                }
            }

            roll->replace( rollPoints );
            pitch->replace( pitchPoints );
            yaw->replace( yawPoints );
        }

        replotCounter = 0;
    }
}
#endif

EulerAngles quaternion2EulerAngles( const gazebo::msgs::Quaternion & q ) {      //Source: https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
    EulerAngles e;

    double ysqr = q.y() * q.y();
    double t0 = -2.0 * (ysqr + q.z() * q.z()) + 1.0;
    double t1 = +2.0 * (q.x() * q.y() + q.w() * q.z());
    double t2 = -2.0 * (q.x() * q.z() - q.w() * q.y());
    double t3 = +2.0 * (q.y() * q.z() + q.w() * q.x());
    double t4 = -2.0 * (q.x() * q.x() + ysqr) + 1.0;

    t2 = t2 > 1.0 ? 1.0 : t2;
    t2 = t2 < -1.0 ? -1.0 : t2;

    e.pitch = std::asin(t2);
    e.roll = std::atan2(t3, t4);
    e.yaw = std::atan2(t1, t0);

    return e;
}

void initPIDControllerState(PIDControllerState *state, double kp, double ki, double kd, double N, double dt)
{
    state->Kp = kp;
    state->Kd = kd;
    state->Ki = ki;
    state->N = N;

    state->ek1 = 0.0;
    state->ek2 = 0.0;
    state->uk1 = 0.0;
    state->uk2 = 0.0;

    state->Ts = dt;
}

double updatePIDController(PIDControllerState *state, double error, double dt)
{
    if( !state ) return 0.0;

    double b0, b1, b2;
    double a0, a1, a2;

    double Kp = state->Kp;
    double Ki = state->Ki;
    double Kd = state->Kd;
    double N = state->N;
    double Ts = dt;

    b0 = (1 + N*Ts)*(Kp + Ki*Ts) + Kd*N;
    b1 = -(Kp*(2 + N*Ts) + Ki*Ts + 2*Kd*N);
    b2 = Kp + Kd*N;

    a0 = (1 + N*Ts);
    a1 = -(2 + N*Ts);
    a2 = 1;

    double k1 = -a1/a0;
    double k2 = -a2/a0;
    double k3 = b0/a0;
    double k4 = b1/a0;
    double k5 = b2/a0;

    double newControlOutput = k1*state->uk1 + k2*state->uk2 + k3*error + k4*state->ek1 + k5*state->ek2;

    state->uk2 = state->uk1;
    state->uk1 = newControlOutput;
    state->ek2 = state->ek1;
    state->ek1 = error;

    return newControlOutput;
}




MotorControlMessage inserir_ajuster_motores( const MotorCommands & cmd, uint16_t rotacao_constante)
{
    static constexpr double PROPELLER_DIAMETER = 10.0;    //[in]
    static constexpr double AIR_DENSITY = 0.00238; //[slugs/ft³]

    static constexpr double CT = 0.0909;
    static constexpr double CP = 0.0362;

    static constexpr double IN_TO_FEET = 1/12.0;
    static constexpr double POUND_2_NEWTON = 4.44822;
    static constexpr double POUNT_FOOT_2_NEWTON_METER = 1.3558;

    static constexpr double RPM_TO_HZ = 1/60.0;
    static constexpr double HZ_TO_RPM = 60.0;

    static constexpr double MOTOR_THRUST_CONSTANT = CT*AIR_DENSITY*pow( (PROPELLER_DIAMETER*IN_TO_FEET), 4)*POUND_2_NEWTON;
    static constexpr double MOTOR_TORQUE_CONSTANT = CP*AIR_DENSITY*pow( (PROPELLER_DIAMETER*IN_TO_FEET), 5)*POUNT_FOOT_2_NEWTON_METER;
    /*------------------------*/
    static constexpr double TORQUE_2_FORCE_CONV = 2.0/(0.5*cos(0.7853981634)); //2/(L*cos(45º))

    double delta_m1 = TORQUE_2_FORCE_CONV*(-cmd.pitchCommand + cmd.rollCommand + cmd.yawCommand);
    double delta_m2 = TORQUE_2_FORCE_CONV*(+cmd.pitchCommand + cmd.rollCommand - cmd.yawCommand);
    double delta_m3 = TORQUE_2_FORCE_CONV*(+cmd.pitchCommand - cmd.rollCommand + cmd.yawCommand);
    double delta_m4 = TORQUE_2_FORCE_CONV*(-cmd.pitchCommand - cmd.rollCommand - cmd.yawCommand);

    double velocidade_m1 = rotacao_constante + delta_m1;
    double velocidade_m2 = rotacao_constante + delta_m2;
    double velocidade_m3 = rotacao_constante + delta_m3;
    double velocidade_m4 = rotacao_constante + delta_m4;

    if(velocidade_m1 < 0)
        velocidade_m1 = 0;

    if(velocidade_m2 < 0)
        velocidade_m2 = 0;

    if(velocidade_m3 < 0)
        velocidade_m3 = 0;

    if(velocidade_m4 < 0)
        velocidade_m4 = 0;

    /*--- Acima calculamos as forças necessarias. ---*/
    velocidade_m1 = sqrt(velocidade_m1/MOTOR_THRUST_CONSTANT)*HZ_TO_RPM;
    velocidade_m2 = sqrt(velocidade_m2/MOTOR_THRUST_CONSTANT)*HZ_TO_RPM;
    velocidade_m3 = sqrt(velocidade_m3/MOTOR_THRUST_CONSTANT)*HZ_TO_RPM;
    velocidade_m4 = sqrt(velocidade_m4/MOTOR_THRUST_CONSTANT)*HZ_TO_RPM;

    /*----------------------------------------------*/

    /*---
     * Abaixo estamos realizando o envio das velocidades dos motores para os mesmos.
     * Supondo a utilização de um ESC cujo DC varia de 0 até 100%, e supondo ainda que,
     * para 100% teremos uma velocidade máxima de 10000 RPM, abaixo iremos convertes o DC
     * calculado acima para a velocidade de cada um dos motores, ou seja, estaremos enviando
     * RPM.
     * ---*/
    MotorControlMessage m;
    m.set_motor1( velocidade_m1 );
    m.set_motor2( velocidade_m2 );
    m.set_motor3( velocidade_m3 );
    m.set_motor4( velocidade_m4 );


    return m;
}

double constrainAngle(double deegreesAngles)
{
    if(deegreesAngles < -180)
        deegreesAngles = (deegreesAngles + 360);
    else if(deegreesAngles > 180)
        deegreesAngles = -(360-deegreesAngles);

    return deegreesAngles;
}

gazebo::msgs::Vector3d operator*( const gazebo::msgs::Vector3d & v1, const float v2 )
{
    gazebo::msgs::Vector3d ans ;

    ans.set_x( v1.x()*v2 );
    ans.set_y( v1.y()*v2 );
    ans.set_z( v1.z()*v2 );

    return ans ;
}

gazebo::msgs::Vector3d operator+( const gazebo::msgs::Vector3d & v1, const gazebo::msgs::Vector3d & v2 ) {
    gazebo::msgs::Vector3d ans ;

    ans.set_x( v1.x() + v2.x() );
    ans.set_y( v1.y() + v2.y() );
    ans.set_z( v1.z() + v2.z() );

    return ans;
}

void DroneControllerGUI::on_comboBox_activated(int index)
{

}
