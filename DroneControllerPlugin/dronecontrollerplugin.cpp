#include "dronecontrollerplugin.h"

gazebo::DroneControllerPlugin::DroneControllerPlugin() :
    m_model( )
  , updateConnection( )
  , m_propulsion( )
{
}

void gazebo::DroneControllerPlugin::OnUpdate( const gazebo::common::UpdateInfo &info )
{
    m_propulsion->update( info );
}
