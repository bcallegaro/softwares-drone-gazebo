#ifndef ATTITTUDEEKF_H
#define ATTITTUDEEKF_H

#include "eigen3/Eigen/Dense"
#include "eigen3/Eigen/Geometry"

#include <QDebug>

#include "iostream"

/*--- Filter Designs. ---*/
//#define SABATINI_2011
#define THALES
/*-----------------------*/

struct KalmanFilterState {
    static constexpr size_t NUMBER_OF_STATES = 4;
    static constexpr size_t NUMBER_OF_MEASURES = 6;
    static constexpr size_t NUMBER_OF_INPUTS = 6;


    typedef Eigen::Matrix< double, NUMBER_OF_STATES, NUMBER_OF_STATES > MatrixNN;
    typedef Eigen::Matrix< double, NUMBER_OF_STATES, 1 > MatrixN1;
    typedef Eigen::Matrix< double, NUMBER_OF_INPUTS, 1 > MatrixB1;
    typedef Eigen::Matrix< double, NUMBER_OF_MEASURES, NUMBER_OF_STATES > MatrixAN;
    typedef Eigen::Matrix< double, NUMBER_OF_MEASURES, NUMBER_OF_MEASURES > MatrixAA;
    typedef Eigen::Matrix< double, NUMBER_OF_MEASURES, 1 > MatrixA1;
    typedef Eigen::Matrix< double, NUMBER_OF_STATES, NUMBER_OF_MEASURES > MatrixNA;

    typedef Eigen::MatrixXf Vector;

    KalmanFilterState( ) :
        X( MatrixN1( 1, 0, 0, 0 ) )
      , F( )
      , H( )
      , p( MatrixNN::Constant( 1e-9 ) )
      , q( )
      , r( )
      , k( ) {

    }

    MatrixN1 X;
    MatrixNN F;
    MatrixAN H;
    MatrixNN p;
    MatrixNN q;
    MatrixAA r;
    MatrixA1 z;
    MatrixA1 measures;
    MatrixA1 y;
    MatrixNA k;

};

Eigen::Matrix<double, 3, 3> getRotMatFromQuaternion( const double & q0, const double & q1, const double & q2, const double & q3 );

class AttittudeEKF
{
public:
    AttittudeEKF();

    void update(Eigen::Vector3d w, Eigen::Vector3d accel, Eigen::Vector3d mag, double dt, double Qquat, double Raccel, double Rmag);

    const KalmanFilterState & state() const { return m_state; }

    KalmanFilterState & rState() { return m_state; }

    Eigen::Matrix4d & updateCovMatrix( ) {
        return m_state.p;
    }

    Eigen::Vector3d m_magRef;
    Eigen::Vector3d m_accelRef;

private:

    KalmanFilterState m_state;

};

#endif // ATTITTUDEEKF_H
