#-------------------------------------------------
#
# Project created by QtCreator 2016-07-13T18:44:58
#
#-------------------------------------------------

QT       -= core gui

CONFIG += c++11

CONFIG += link_pkgconfig

TARGET = DroneControllerPlugin
TEMPLATE = lib

SOURCES += dronecontrollerplugin.cpp \
    dronepropulsiondriver.cpp \
    ../messages/MotorControlMessage.pb.cc

HEADERS += dronecontrollerplugin.h \
    dronepropulsiondriver.h \
    ../messages/MotorControlMessage.pb.h

DESTDIR += ../build

# Messages
INCLUDEPATH += ../messages
DEPENDPATH  += ../messages

#Catkin Dirs
CATKIN_LIBS_DIR = /usr/lib/x86_64-linux-gnu
CATKIN_INCLUDE_DIR = /usr/include/
CATKIN_GAZEBO_INCLUDE_DIR = /usr/include/gazebo-8

#Gazebo include pahts.
INCLUDEPATH += $${CATKIN_GAZEBO_INCLUDE_DIR}/
INCLUDEPATH += $${CATKIN_GAZEBO_INCLUDE_DIR}/gazebo/transport
INCLUDEPATH += $${CATKIN_GAZEBO_INCLUDE_DIR}/gazebo/msgs/
INCLUDEPATH += $${CATKIN_INCLUDE_DIR}/ignition/msgs0/
INCLUDEPATH += $${CATKIN_INCLUDE_DIR}/sdformat-5.2
INCLUDEPATH += $${CATKIN_INCLUDE_DIR}/ignition/math3
INCLUDEPATH += $${CATKIN_INCLUDE_DIR}/ignition/transport3

PKGCONFIG += sdformat

#Library path for catkin compiled librarys
LIBS += -L$${CATKIN_LIBS_DIR}

#Including alglib library
LIBS += -lalglib

#Ignition includes
LIBS += -lignition-math3
LIBS += -lignition-transport3
LIBS += -lsdformat

#Gazebo libraries
LIBS += -lgazebo
LIBS += -lgazebo_client
LIBS += -lgazebo_transport
LIBS += -lgazebo_msgs
LIBS += -lgazebo_common

