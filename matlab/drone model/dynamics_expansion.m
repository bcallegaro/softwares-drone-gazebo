clear all;
close all;
clc;

%% Defnindo variáveis simbólicas que serão utilizadas
syms L F1 F2 F3 F4 M1 M2 M3 M4 wx wy wz Ixx Iyy Izz U1 U2 U3 Ts real;
syms wx_dot wy_dot wz_dot real;
syms phi theta psi real;

%% Definindo vetores e matrizes que serão utilizadas
I = [   Ixx 0 0; 
        0 Iyy 0; 
        0 0 Izz];   %Momento de inercia no referencial do corpo

omega = [   wx;
            wy;
            wz];    %Velocidades angulares no referencial do corpo (gyro).
        
        
%% Equação dinâmica. (Baseada no cousera)

% u = [   F2 - F4;
%         F3 - F1;
%         M1 - M2 + M3 - M4];

 u = [   U1;
         U2;
         U3     ];

omega_dot = I\( u - cross(omega, I*omega)); %Velocidade angualar no frame do veículo
 

%% Equação de conversão de velocidades (Body Frame-> Fixed )

vel_change_matrix = [   cos(theta)  0           sin(theta);
                        0           1           0
                        -cos(phi)   sin(phi)   cos(phi)*cos(theta)  ];

%% Escrevendo a expressão de estados

f = [   vel_change_matrix*[ wx; 
                            wy 
                            wz];
        omega_dot ];
        
x = [phi, theta, psi, wx, wy, wz]';

inputs = [U1, U2, U3]';


%% Linearização
% A = jacobian(f, x' );
 B = jacobian(f, inputs' );
 
 % Alteração, utilizando a linearização proposta no artigo (Full Linear
 % Control of a Quadrotor UAV, LQ vs H∞)
 
 A = [  zeros(3,3), eye(3);
        zeros(3,3), zeros(3,3) ];

%% Algumas suposições (ângulos de roll e pitch sempre entre -15 e 15 

% Primeiro - Como os ängulos ficaram entre -15 e 15 graus, cos(x) = 1 e
% sin(x) = 1. (Não muito válido fora de +-10 graus, entretanto, usaremos
% como forma de prosseguir).
A = subs(A, [cos(phi), sin(phi) ], [1, phi ] );
A = subs(A, [cos(theta), sin(theta) ], [1, theta ] );

A = subs(A, [wx, wy, wz], [0, 0, 0] );


%% Expandido o estado para utilizar a parte de integração
syms phi_ref theta_ref psi_ref real

% O vetor abaixo será construído com a referência dos ângulos e as
% velocidades será ajustada como sempre sendo desejada zero
ref = [phi_ref; theta_ref; psi_ref];

% A matriz de observação será composta por identidades. Isso simplificara
% essa simulação. Para o drone, as velocidades são realmente medidas e os
% ângulos obtidos do filtro de Kalman, logo, para simplificar, iremos tomar
% como se C = [eye, 0; 0 eye]
C = [   eye(3),     zeros(3,3);
        zeros(3,3), eye(3)      ];
    
% Expandindo o estado
Ae = [  A,  zeros(6,3);
        -eye(3), zeros(3,6)];
    
Be = [  B; 
        zeros(3,3) ];
    
Ce = [ C, zeros(6,3) ];

De = zeros(6,6);

xe = [x; ref];
% O 0.controle sera da forma u = -Ke*xe.2200

%Ke = [K; Ki] ->1 Ki será o ganho int100.644egral. K 6000será o ganho obtido através da
%funç2ão lqr do matlab30 para os estados originais linearizados (A, B )

%% Su2bstituindo valores e obROTATE_REF_VECTORtendo as funções de peso
Q = eye(6,6);
R = eye(3,3);

Q(1,1) = 3*10^0;
Q(2,2) = 3*10^0;
Q(3,3) = 1*10^0;
Q(4,4) = 2*10^-2;
Q(5,5) = 2*10^-2;
Q(6,6) = 3*10^-1;

R(1,1) = 0.1;
R(2,2) = 0.1;
R(3,3) = 0.01;

A_val = double( A ); %Sem valores para substituição.
B_val = double( subs(B, [Ixx, Iyy, Izz], [0.0099, 0.0099, 0.0151] ) );

%% Calculando a matriz de ganhos K Lqr (Sistema discreto)
Ts = 1/400;
K = lqrd(A_val, B_val, Q, R, 0.0025);
Ki = eye(3,3);
Ke = [K, Ki];

%% Montando o sistema realimentado e aumentado e fazendo alguns testes preliminares
Ae = [  A_val,  zeros(6,3);
        zeros(3), zeros(3,6)];

    
Be = [  B_val; 
        zeros(3,3) ];
    
%% Calculando a matriz de ganhos K Lqr (Sistema continuo)
K = lqr(A_val, B_val, Q, R);
Ki = eye(3,3);
Ke = [K, Ki];

sys = ss((Ae-Be*Ke), Be, Ce, zeros(6,3));

t = 0:0.01:30;
r = zeros(3, size(t,2));
r(1,:) = 0.261799*ones(size(t));

[y,t,x]=lsim(sys,r,t);
plot(t, x)
figure
plot(t,y(:,1));