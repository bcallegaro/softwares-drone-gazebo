#ifndef DRONECONTROLLERPLUGIN_H
#define DRONECONTROLLERPLUGIN_H


/*--- STL Includes. ---*/
#include <memory>
#include <iostream>

/*--- Gazebo includes. ---*/
#include <sdf/sdf.hh>
#include "gazebo/gazebo.hh"
#include "gazebo/common/Plugin.hh"
#include "gazebo/msgs/msgs.hh"
#include "gazebo/physics/physics.hh"
#include "gazebo/physics/Link.hh"
#include "gazebo/transport/transport.hh"
#include "gazebo/transport/Node.hh"
#include "gazebo/transport/Subscriber.hh"

/*--- Qt Includes. ---*/


/*--- My drivers. ---*/
#include "dronepropulsiondriver.h"

/*--- My Messages. ---*/
#include "MotorControlMessage.pb.h"

//typedef const boost::shared_ptr<gazebo::msgs::Vector3d const> ConstVector3dPtr;
typedef const boost::shared_ptr< MotorControlMessage const > ConstMotorControlMessagePtr;


namespace gazebo {
class DroneControllerPlugin : public ModelPlugin
{

public:
    DroneControllerPlugin();

    void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf) {
        this->m_model = _parent;

        this->updateConnection = event::Events::ConnectWorldUpdateBegin(
                    boost::bind(&DroneControllerPlugin::OnUpdate, this, _1));

        gazebo::transport::NodePtr node(new gazebo::transport::Node());
        node->Init();

        gazebo::transport::run();

        sub = node->Subscribe("~/drone/motors", &DroneControllerPlugin::motorsCallback, this );

        m_propulsion.reset( new DronePropulsionDriver( m_model ) );
    }

private:
    /**
     * @brief OnUpdate
     * Method called on every simulation step update. It carry all the model and
     * simulation properties.
     */
    void OnUpdate(const common::UpdateInfo & info);

    void motorsCallback( ConstMotorControlMessagePtr &_msg) {
        m_propulsion->setReference( _msg->motor1(), _msg->motor2(), _msg->motor3(), _msg->motor4() );
    }


    physics::ModelPtr m_model;

    event::ConnectionPtr updateConnection;

    gazebo::transport::NodePtr node;
    gazebo::transport::SubscriberPtr sub;

    /*--- Drivers. ---*/
    std::unique_ptr< DronePropulsionDriver > m_propulsion;
};

    GZ_REGISTER_MODEL_PLUGIN(DroneControllerPlugin)
}
#endif // DRONECONTROLLERPLUGIN_H
