clear all;
close all;
clc;

%Leitura do arquivo
output = csvread( 'imu_data21-44-17y-10-11-16.csv',1, 0 );

time = output(:,1);

qw_ref = output(:,2);
qx_ref = output(:,3);
qy_ref = output(:,4);
qz_ref = output(:,5);

gx = output(:,6);
gy = output(:,7);
gz = output(:,8);

ax = output(:,9);
ay = output(:,10);
az = output(:,11);

hx = output(:,12);
hy = output(:,13);
hz = output(:,14);

lenght = size(qw_ref, 1);

roll = zeros( lenght, 1 );
pitch = zeros( lenght, 1);
yaw = zeros( lenght, 1 );

accel_rotated = zeros( lenght, 3 );
mag_rotated = zeros( lenght, 3 );

for i = 1:lenght
    q = [qw_ref(i), qx_ref(i), qy_ref(i), qz_ref(i)];
    %[roll(i), pitch(i), yaw(i)] = quat2angle( q, 'XYZ' );
    
    rotm = quat2rotm( q(1), q(2), q(3), q(4) );
    
    accel_rotated(i, : ) = (rotm*[0, 0, 9.81]')';
    mag_rotated(i, : ) = ( rotm*[5.99667e-6, 2.29981e-5, -4.20015e-5]')';
    
    
end

hold on
plot( time, ax, 'b' )
plot( time, ay, 'r' )
plot( time, az, 'g' )

plot( time, accel_rotated(:,1), 'b--' )
plot( time, accel_rotated(:,2), 'r--' )
plot( time, accel_rotated(:,3), 'g--' )

legend( 'ax', 'ay', 'az', 'ax_r', 'ay_r', 'az_r' )

figure; hold on
plot( time, hx, 'b' )
plot( time, hy, 'r' )
plot( time, hz, 'g' )

plot( time, mag_rotated(:,1), 'b--' )
plot( time, mag_rotated(:,2), 'r--' )
plot( time, mag_rotated(:,3), 'g--' )

legend( 'hx', 'hy', 'hz', 'hx_r', 'hy_r', 'hz_r' )

syms gx gy gz hx hy hz q0 q1 q2 q3 real;

q_vec = [q1, q2, q3];

q = [q0, q_vec];

g = [gx; gy; gz];
h = [hx; hy; hz];

rotm = quat2rotm( q(1), q(2), q(3), q(4) );

rotatedg = rotm*g;
rotatedh = rotm*h;

jacobian( rotatedg, [q0, q1, q2, q3] )
jacobian( rotatedh, [q0, q1, q2, q3] )



