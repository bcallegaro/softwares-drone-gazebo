function rotm = quat2rotm( q0 , q1, q2, q3 )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

rotm =  [ - 2*q2^2 - 2*q3^2 + 1,     2*q0*q3 + 2*q1*q2,     2*q1*q3 - 2*q0*q2;
          2*q1*q2 - 2*q0*q3, - 2*q1^2 - 2*q3^2 + 1,     2*q0*q1 + 2*q2*q3;
          2*q0*q2 + 2*q1*q3,     2*q2*q3 - 2*q0*q1, - 2*q1^2 - 2*q2^2 + 1];

% rotm = [ q1^2 - q2^2 - q3^2 + q4^4  2*(q1*q2+q3*q4) 2*(q1*q3-q2*q4);
%          2*(q1*q2-q3*q4) -q1^2 + q2^2 - q3^2 + q4^2 2*(q2*q3+q4*q1);
%          2*(q1*q3 + q2*q4) 2*(q2*q3-q4*q1) -q1^2-q2^2+q3^2+q4^2];
     
%rotm = rotm*1/sqrt((q1^2+q2^2+q3^2) + q4^2);
end

