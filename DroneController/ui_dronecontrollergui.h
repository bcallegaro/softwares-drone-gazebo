/********************************************************************************
** Form generated from reading UI file 'dronecontrollergui.ui'
**
** Created by: Qt User Interface Compiler version 5.9.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DRONECONTROLLERGUI_H
#define UI_DRONECONTROLLERGUI_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DroneControllerGUI
{
public:
    QWidget *centralWidget;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *plotVerticalLayout;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QComboBox *comboBox;
    QSpacerItem *verticalSpacer;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *DroneControllerGUI)
    {
        if (DroneControllerGUI->objectName().isEmpty())
            DroneControllerGUI->setObjectName(QStringLiteral("DroneControllerGUI"));
        DroneControllerGUI->resize(903, 849);
        centralWidget = new QWidget(DroneControllerGUI);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayoutWidget = new QWidget(centralWidget);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(220, 10, 671, 791));
        plotVerticalLayout = new QVBoxLayout(verticalLayoutWidget);
        plotVerticalLayout->setSpacing(6);
        plotVerticalLayout->setContentsMargins(11, 11, 11, 11);
        plotVerticalLayout->setObjectName(QStringLiteral("plotVerticalLayout"));
        plotVerticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayoutWidget_2 = new QWidget(centralWidget);
        verticalLayoutWidget_2->setObjectName(QStringLiteral("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(40, 19, 160, 271));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(verticalLayoutWidget_2);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout->addWidget(label);

        comboBox = new QComboBox(verticalLayoutWidget_2);
        comboBox->setObjectName(QStringLiteral("comboBox"));

        verticalLayout->addWidget(comboBox);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        DroneControllerGUI->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(DroneControllerGUI);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 903, 22));
        DroneControllerGUI->setMenuBar(menuBar);
        mainToolBar = new QToolBar(DroneControllerGUI);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        DroneControllerGUI->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(DroneControllerGUI);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        DroneControllerGUI->setStatusBar(statusBar);

        retranslateUi(DroneControllerGUI);

        QMetaObject::connectSlotsByName(DroneControllerGUI);
    } // setupUi

    void retranslateUi(QMainWindow *DroneControllerGUI)
    {
        DroneControllerGUI->setWindowTitle(QApplication::translate("DroneControllerGUI", "DroneControllerGUI", Q_NULLPTR));
        label->setText(QApplication::translate("DroneControllerGUI", "Fonte de Orienta\303\247\303\243o", Q_NULLPTR));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("DroneControllerGUI", "Reference", Q_NULLPTR)
         << QApplication::translate("DroneControllerGUI", "Kalman Filtered", Q_NULLPTR)
         << QApplication::translate("DroneControllerGUI", "ComplementaryFilter", Q_NULLPTR)
        );
    } // retranslateUi

};

namespace Ui {
    class DroneControllerGUI: public Ui_DroneControllerGUI {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DRONECONTROLLERGUI_H
