#include "dronepropulsiondriver.h"

DronePropulsionDriver::DronePropulsionDriver(gazebo::physics::ModelPtr model) :
    m_lastSimTime( 0.0f )
  , m_motor1Ref( 0.0f )
  , m_motor2Ref( 0.0f )
  , m_motor3Ref( 0.0f )
  , m_motor4Ref( 0.0f )
  , m_motor1Speed( 0.0f )
  , m_motor2Speed( 0.0f )
  , m_motor3Speed( 0.0f )
  , m_motor4Speed( 0.0f )
  , m_motors( )
  , m_propellers( )
  , m_motorsControllers( )
  , m_receptionCounter( 0 )
{
    /*--- Setting up vectors size. ---*/
    m_motors.resize( 4 );
    m_propellers.resize( 4 );
    m_motorsControllers.resize( 4 );
    m_motorsTop.resize( 4 );

    setupControllers();

    /*--- Getting references. ---*/
    m_motors[ 0 ] = model->GetJoint( "motorAxis_1" );
    m_motors[ 1 ] = model->GetJoint( "motorAxis_2" );
    m_motors[ 2 ] = model->GetJoint( "motorAxis_3" );
    m_motors[ 3 ] = model->GetJoint( "motorAxis_4" );

    m_propellers[ 0 ] = model->GetLink( "propeller_1" );
    m_propellers[ 1 ] = model->GetLink( "propeller_2" );
    m_propellers[ 2 ] = model->GetLink( "propeller_3" );
    m_propellers[ 3 ] = model->GetLink( "propeller_4" );

    m_motorsTop[0] = model->GetLink( "motorTop_1" );
    m_motorsTop[1] = model->GetLink( "motorTop_2" );
    m_motorsTop[2] = model->GetLink( "motorTop_3" );
    m_motorsTop[3] = model->GetLink( "motorTop_4" );

}

void DronePropulsionDriver::update( const gazebo::common::UpdateInfo &info )
{
    gazebo::common::Time dt = info.simTime - m_lastSimTime;
    m_lastSimTime = info.simTime;

    using namespace gazebo::common;

    ++m_receptionCounter;

    if( m_receptionCounter > 5 ) {
        m_motor1Ref = m_motor2Ref = m_motor3Ref = m_motor4Ref = 0;
    }

    /*--- Simula dinâmica de aceleração do motor + esc (Primeira ordem). ---*/
    m_motor1Speed = m_motor1Speed*0.9 + m_motor1Ref*0.1;
    m_motor2Speed = m_motor2Speed*0.9 + m_motor2Ref*0.1;
    m_motor3Speed = m_motor3Speed*0.9 + m_motor3Ref*0.1;
    m_motor4Speed = m_motor4Speed*0.9 + m_motor4Ref*0.1;

    double hzMotor1Speed, hzMotor2Speed, hzMotor3Speed, hzMotor4Speed;

    hzMotor1Speed = m_motor1Speed*RPM_TO_HZ;
    hzMotor2Speed = m_motor2Speed*RPM_TO_HZ;
    hzMotor3Speed = m_motor3Speed*RPM_TO_HZ;
    hzMotor4Speed = m_motor4Speed*RPM_TO_HZ;

    double motor1Torque = pow( hzMotor1Speed, 2)*MOTOR_TORQUE_CONSTANT;
    double motor2Torque = pow( hzMotor2Speed, 2)*MOTOR_TORQUE_CONSTANT;
    double motor3Torque = pow( hzMotor3Speed, 2)*MOTOR_TORQUE_CONSTANT;
    double motor4Torque = pow( hzMotor4Speed, 2)*MOTOR_TORQUE_CONSTANT;

    m_motorsTop.at( 0 )->AddRelativeTorque( gazebo::math::Vector3( 0, -motor1Torque, 0  ) );
    m_motorsTop.at( 1 )->AddRelativeTorque( gazebo::math::Vector3( 0, +motor2Torque, 0  ) );
    m_motorsTop.at( 2 )->AddRelativeTorque( gazebo::math::Vector3( 0, -motor3Torque, 0  ) );
    m_motorsTop.at( 3 )->AddRelativeTorque( gazebo::math::Vector3( 0, +motor4Torque, 0  ) );

    double motor1Thurst = pow( hzMotor1Speed, 2)*MOTOR_THRUST_CONSTANT;
    double motor2Thurst = pow( hzMotor2Speed, 2)*MOTOR_THRUST_CONSTANT;
    double motor3Thurst = pow( hzMotor3Speed, 2)*MOTOR_THRUST_CONSTANT;
    double motor4Thurst = pow( hzMotor4Speed, 2)*MOTOR_THRUST_CONSTANT;

    m_propellers.at( 0 )->AddRelativeForce( gazebo::math::Vector3( 0, motor1Thurst, 0 ) );
    m_propellers.at( 1 )->AddRelativeForce( gazebo::math::Vector3( 0, motor2Thurst, 0 ) );
    m_propellers.at( 2 )->AddRelativeForce( gazebo::math::Vector3( 0, motor3Thurst, 0 ) );
    m_propellers.at( 3 )->AddRelativeForce( gazebo::math::Vector3( 0, motor4Thurst, 0 ) );

}

void DronePropulsionDriver::setForcesAndTorques()
{

}

void DronePropulsionDriver::setupControllers()
{
    for( size_t i = 0; i < m_motorsControllers.size(); ++i ) {
        m_motorsControllers.at( i ).Init( 0.005f, 0.001f, 0.0f );
    }

}

void DronePropulsionDriver::setReference(double m1Ref, double m2Ref, double m3Ref, double m4Ref)
{
    m_receptionCounter = 0;

    m_motor1Ref = m1Ref;
    m_motor2Ref = m2Ref;
    m_motor3Ref = m3Ref;
    m_motor4Ref = m4Ref;
}
