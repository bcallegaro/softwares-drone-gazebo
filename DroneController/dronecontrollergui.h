#ifndef DRONECONTROLLERGUI_H
#define DRONECONTROLLERGUI_H

#include <iostream>

#include <QMainWindow>

#define CHARTS_ENABLED

#if defined(CHARTS_ENABLED)

#include <QChartGlobal>

QT_CHARTS_BEGIN_NAMESPACE
class QValueAxis;
class QChart;
class QChartView;
class QLineSeries;
QT_CHARTS_END_NAMESPACE

QT_CHARTS_USE_NAMESPACE
#endif

#include <QFile>
#include <QPointF>
#include <QVector>

#include "gazebo/gazebo.hh"
#include "gazebo/gazebo_client.hh"

#include "attittudeekf.h"

/*--- Joystick includes. ---*/
#include "../../../joystick/joystick.hh"

/*--- My Messages. ---*/
#include "MotorControlMessage.pb.h"
#include "firfilter.h"

/*--- Data Source. ---*/
//#define RAW_SIMULATED_SOURCE
#define ROTATE_REF_VECTOR
/*--------------------*/

/*--- Machine. ---*/
#define DRONE
//#define INERTIAL_PLATFORM
/*----------------*/

/*--- Controller. ---*/
//#define PID
#define LQRI
/*-------------------*/

struct EulerAngles {
    EulerAngles( double _roll = 0.0, double _pitch = 0.0, double _yaw = 0.0 ) :
        roll( _roll )
      , pitch( _pitch )
      , yaw( _yaw ) {

    }

    double roll;
    double pitch;
    double yaw;
};

struct Axes {
    double x, y, z;
};

struct MotorCommands {
    double pitchCommand;
    double rollCommand;
    double yawCommand;
};

typedef Axes Rate;
typedef Axes RateRef;

EulerAngles quaternion2EulerAngles( const gazebo::msgs::Quaternion & q );

static constexpr double DC_TO_RPM = 100.0;
MotorControlMessage inserir_ajuster_motores(const MotorCommands &cmd, uint16_t rotacao_constante);

double constrainAngle(double deegreesAngles);

/*--- PID Structures. ---*/
typedef struct {
    double Kp;
    double Ki;
    double Kd;
    double N;
    double Ts;

    double uk1; //Ação de controle anterior u[k-1];
    double uk2; //Acção de controle de duas iterações passadas u[k-2];
    double ek1; //Erro anterior e[k-1];
    double ek2; //Erro de duas iterações anteriores e[k-2];
} PIDControllerState;

void initPIDControllerState(PIDControllerState *state, double kp, double ki, double kd, double N, double dt);
double updatePIDController(PIDControllerState *state, double error, double dt);
/*------------------------*/

/*--- LQR+I Structures. ---*/
using LQRIVector = Eigen::Matrix<double, 3, 1 >;
using LQRGain = Eigen::Matrix<double, 3, 9 >;
using LQRIOutput = Eigen::Matrix<double, 3, 1 >;
using LQRStateVector = Eigen::Matrix<double, 9, 1 >;

typedef struct {
    LQRGain kMatrix;
    LQRIVector iState;
} LQRIController;

/*-------------------------*/

gazebo::msgs::Vector3d operator*( const gazebo::msgs::Vector3d & v1, const float v2 );
gazebo::msgs::Vector3d operator+( const gazebo::msgs::Vector3d & v1, const gazebo::msgs::Vector3d & v2 );

namespace Ui {
class DroneControllerGUI;
}

class DroneControllerGUI : public QMainWindow
{
    Q_OBJECT

    enum AttitudeIndex {
        ROLL,
        PITCH,
        YAW
    };

    enum Controller {
        RefAttitudeController,
        RawEkfAttitudeController,
        FirEkfAttitudeController,
        ComplementaryFilterAttitudeController
    };

public:
    explicit DroneControllerGUI(QWidget *parent = 0);
    ~DroneControllerGUI();

private slots:
    void on_comboBox_activated(int index);

private:
    /*--- interface config. ---*/
    //void setupPlots(QCustomPlot *plot, float yMaxLimit, float yMinLimit );


#if defined(CHARTS_ENABLED)
    void plot3DVectorData( QtCharts::QChartView *plot, const gazebo::msgs::Vector3d &data, double time, uint8_t & replotCounter );
    void plot3DVectorData( QtCharts::QChartView *plot, QLineSeries *roll, QLineSeries *pitch, QLineSeries *yaw, const EulerAngles &angles, double time, double startTime, uint8_t & replotCounter );
#endif

    //void setupPlots( QtCharts::QChartView *chart, float yMaxLimit, float yMinLimit );

    void newIMUDataReceived( ConstIMUPtr & imuData );
    void newAltimeterDataReceived( ConstAltimeterPtr &altitude );
    void newMagnetometerDataReceived( ConstMagnetometerPtr &magnetometer );

    MotorCommands getPIDControlOutputs(const gazebo::msgs::Quaternion &ref, const gazebo::msgs::IMU &imuData , Controller controller, double dt);
    MotorCommands getLQRIControlOutputs(const gazebo::msgs::Quaternion &ref, const gazebo::msgs::IMU &imuData , Controller controller, double dt);

    void timerEvent( QTimerEvent *e );

    Ui::DroneControllerGUI *ui;

    double m_altitudeRef;
    double m_rollRef;
    double m_pitchRef;
    double m_yawRef;

    gazebo::transport::PublisherPtr m_motorPublisher;
    gazebo::transport::SubscriberPtr m_imuSubs;
    gazebo::transport::SubscriberPtr m_altimeterSubs;
    gazebo::transport::SubscriberPtr m_magnetometerSubs;

    gazebo::msgs::IMU m_filteredIMU;
    gazebo::msgs::IMU m_rawIMU;

    Eigen::Vector3d m_magnetometer;

    MotorControlMessage m_zeroMessage;

    /*--- filters. ---*/
    std::unique_ptr < FIRFilter< gazebo::msgs::Vector3d > > m_gyroFilter;
    std::unique_ptr< FIRFilter< gazebo::msgs::Vector3d > > m_accelFilter;

    PIDControllerState m_altitudeController;


    double m_altitudeCmd;
    double m_altitude;

    bool m_controllersInitialized;
    std::vector< std::vector< PIDControllerState > > m_pidAttitudeControllers ;

    std::vector< LQRIController > m_lqrAttitudeControllers ;

    gazebo::common::Time m_altimterLastTs;
    gazebo::common::Time m_imuLastTs;

    /*--- Filtro de Kalman para a posição angular. ---*/
    AttittudeEKF m_firAttitudeEkf;
    AttittudeEKF m_rawAttitudeEkf;

    /*--- variaveis de controle de inicialização. ---*/
    bool m_firstAccelReceived;
    bool m_firstMagReceived;
    bool m_filtersInitialized;

    /*--- Timers ---*/
    quint8 m_joystickTimerId;

    Joystick *m_joystick;

#if defined(CHARTS_ENABLED)
    /*--- Variáveis para os gráficos. ---*/
    QChartView *m_refChartView;
    QChartView *m_curChartView;

    QLineSeries *m_rollRefSeries;
    QLineSeries *m_pitchRefSeries;
    QLineSeries *m_yawRefSeries;

    QLineSeries *m_rollEstSeries;
    QLineSeries *m_pitchEstSeries;
    QLineSeries *m_yawEstSeries;
#endif

    gazebo::msgs::Quaternion updateEKF(gazebo::msgs::IMU & imu, Eigen::Vector3d & mag, double dt, AttittudeEKF & filter , bool motorsEnabled = false );
    const EulerAngles & updateCF(gazebo::msgs::IMU & imu, Eigen::Vector3d & mag, double dt, AttittudeEKF & filter , bool motorsEnabled = false );
};

/*

FIR filter designed with
http://t-filter.appspot.com

sampling frequency: 400 Hz

* 0 Hz - 70 Hz
  gain = 1
  desired ripple = 0.1 dB
  actual ripple = 0.058362386338179736 dB

* 90 Hz - 200 Hz
  gain = 0
  desired attenuation = -40 dB
  actual attenuation = -42.030482595559626 dB

*/

#define FILTER_TAP_NUM 47

static double filter_taps[FILTER_TAP_NUM] = {
  -0.0039005863172220686,
  0.002077711478000766,
  0.004327547535449486,
  0.0013442350845899868,
  -0.004791877907148006,
  -0.004906351003127644,
  0.0037084060309327135,
  0.009745632073333031,
  0.0019068277499777156,
  -0.011793251116987612,
  -0.010557485887868086,
  0.008843757743404974,
  0.020566063045385406,
  0.0024344777782872033,
  -0.026240385582264724,
  -0.02174299453827226,
  0.02136053212721833,
  0.046749183110544684,
  0.002823488200074752,
  -0.07165291125895973,
  -0.0632165856509606,
  0.09023631830468237,
  0.3028750920651672,
  0.40296591025098893,
  0.3028750920651672,
  0.09023631830468237,
  -0.0632165856509606,
  -0.07165291125895973,
  0.002823488200074752,
  0.046749183110544684,
  0.02136053212721833,
  -0.02174299453827226,
  -0.026240385582264724,
  0.0024344777782872033,
  0.020566063045385406,
  0.008843757743404974,
  -0.010557485887868086,
  -0.011793251116987612,
  0.0019068277499777156,
  0.009745632073333031,
  0.0037084060309327135,
  -0.004906351003127644,
  -0.004791877907148006,
  0.0013442350845899868,
  0.004327547535449486,
  0.002077711478000766,
  -0.0039005863172220686
};







#endif // DRONECONTROLLERGUI_H
